import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import contactCtrl from '../controllers/contact.controller';

const router = express.Router(); // eslint-disable-line new-cap

//const config = require('../../config/env');

router.route('/')
  /** GET /api/contact - Get list of contact */
  .get(contactCtrl.list)

  /** POST /api/savecontact - Add contact */
  .post(contactCtrl.create);


  export default router;
