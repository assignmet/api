import express from 'express';
import userRoutes from './user.route';
import authRoutes from './auth.route';
import contactRoutes from './contact.route';

const router = express.Router(); // eslint-disable-line new-cap

/** GET /health-check - Check service health */
router.get('/health-check', (req, res) =>
  res.send('OK')
);

// mount user routes at /users
router.use('/users', userRoutes);

// mount auth routes at /auth
router.use('/auth', authRoutes);

// mount contact routs at /contacts
router.use('/contacts', contactRoutes);

export default router;
