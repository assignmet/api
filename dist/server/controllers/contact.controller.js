'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _contact = require('../models/contact.model');

var _contact2 = _interopRequireDefault(_contact);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Get contact us.
 * @property {number} req.query.skip - Number of users to be skipped.
 * @property {number} req.query.limit - Limit number of users to be returned.
 * @returns {User[]}
 */

function list(req, res, next) {
  var _req$query = req.query,
      _req$query$limit = _req$query.limit,
      limit = _req$query$limit === undefined ? 50 : _req$query$limit,
      _req$query$skip = _req$query.skip,
      skip = _req$query$skip === undefined ? 0 : _req$query$skip;

  _contact2.default.list({
    limit: limit,
    skip: skip
  }).then(function (list) {
    return res.json(list);
  }).catch(function (e) {
    return next(e);
  });
}

function create(req, res, next) {
  var contact = new _contact2.default({
    first_name: req.body.fName,
    last_name: req.body.lName,
    email: req.body.email,
    phone: req.body.phonenumber

  });
  contact.save().then(function (savedContact) {
    return res.json(savedContact);
  }).catch(function (e) {
    return next(console.log(e));
  });
}

exports.default = {
  list: list,
  create: create
};
module.exports = exports['default'];
//# sourceMappingURL=contact.controller.js.map
