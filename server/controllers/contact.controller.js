import Contact from '../models/contact.model';

/**
 * Get contact us.
 * @property {number} req.query.skip - Number of users to be skipped.
 * @property {number} req.query.limit - Limit number of users to be returned.
 * @returns {User[]}
 */

function list(req, res, next) {
  const {
    limit = 50, skip = 0
  } = req.query;
  Contact.list({
      limit,
      skip
    })
    .then(
      list => res.json(list)
    )
    .catch(e => next(e));
}

function create(req, res, next) {
  const contact = new Contact({
    first_name: req.body.fName,
    last_name: req.body.lName,
    email: req.body.email,
    phone: req.body.phonenumber,

  });
  contact.save()
    .then(
      savedContact => res.json(savedContact)
    )
    .catch(e => next(console.log(e)));
}

export default {
  list,
  create
};
