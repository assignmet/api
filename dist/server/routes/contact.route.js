'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _expressValidation = require('express-validation');

var _expressValidation2 = _interopRequireDefault(_expressValidation);

var _paramValidation = require('../../config/param-validation');

var _paramValidation2 = _interopRequireDefault(_paramValidation);

var _contact = require('../controllers/contact.controller');

var _contact2 = _interopRequireDefault(_contact);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router(); // eslint-disable-line new-cap

//const config = require('../../config/env');

router.route('/')
/** GET /api/contact - Get list of contact */
.get(_contact2.default.list)

/** POST /api/savecontact - Add contact */
.post(_contact2.default.create);

exports.default = router;
module.exports = exports['default'];
//# sourceMappingURL=contact.route.js.map
